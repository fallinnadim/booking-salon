package com.booking.service;

import java.util.List;
import com.booking.models.Service;

public class ServiceService {
    public static Service getServiceByServiceId(List<Service> serviceList, String serviceId) {
        return serviceList
                .stream()
                .filter(element -> element.getServiceId().equalsIgnoreCase(serviceId))
                .findFirst()
                .orElse(null);
    }

    public static List<Service> getAllService(List<Service> serviceList) {
        return serviceList;
    }

}
