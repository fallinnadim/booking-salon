package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;

public class MenuService {
    private static List<Person> personList = PersonRepository.getAllPerson();
    private static List<Service> serviceList = ServiceRepository.getAllService();
    private static List<Reservation> reservationList = new ArrayList<>();
    private static Scanner input = new Scanner(System.in);

    public static void mainMenu() {
        String[] mainMenuArr = { "Show Data", "Create Reservation", "Complete/cancel reservation", "Exit" };
        String[] subMenuArr = { "Recent Reservation", "Show Customer", "Show Available Employee",
                "Show Reservation History", "Back to main menu" };
        int optionMainMenu;
        int optionSubMenu;

        boolean backToMainMenu = false;
        boolean backToSubMenu = false;
        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            optionMainMenu = ValidationService.validateInputMenu(input.nextLine(), 3);
            switch (optionMainMenu) {
                case 1:
                    do {
                        PrintService.printMenu("Show Data", subMenuArr);
                        optionSubMenu = ValidationService.validateInputMenu(input.nextLine(), 4);
                        // Sub menu - menu 1
                        switch (optionSubMenu) {
                            case 1:
                                // panggil fitur tampilkan recent reservation
                                PrintService.showRecentReservation(ReservationService
                                        .getRecentReservation(reservationList));
                                backToSubMenu = false;
                                break;
                            case 2:
                                // panggil fitur tampilkan semua customer
                                PrintService.showAllCustomer(CustomerService.getAllCustomer(personList));
                                backToSubMenu = false;
                                break;
                            case 3:
                                // panggil fitur tampilkan semua employee
                                PrintService.showAllEmployee(EmployeeService.getAllEmployee(personList));
                                backToSubMenu = false;
                                break;
                            case 4:
                                // panggil fitur tampilkan history reservation + total keuntungan
                                PrintService.showHistoryReservation(ReservationService
                                        .getHistoryReservation(reservationList));
                                backToSubMenu = false;
                                break;
                            case -1:
                                System.out.println("input harus berupa angka antara 0 sampai 4\n");
                                break;
                            case 0:
                                backToSubMenu = true;
                        }
                    } while (!backToSubMenu);
                    break;
                case 2:
                    // panggil fitur menambahkan reservation
                    Customer customer = ValidationService.validateCustomer(getInputCustomer());
                    Employee employee = ValidationService.validateEmployee(getInputEmployee());
                    List<Service> newService = getListService(serviceList);
                    ReservationService.createReservation(reservationList, customer, employee, newService);
                    System.out.println("Booking Berhasil");
                    backToSubMenu = false;
                    break;
                case 3:
                    // panggil fitur mengubah workstage menjadi finish/cancel
                    if (ReservationService.getRecentReservation(reservationList).size() == 0) {
                        System.out.println("+========================DATA TIDAK DITEMUKAN=========================+");
                        backToSubMenu = false;
                        break;
                    }
                    boolean reservationOK = false;
                    Reservation reservation = new Reservation();
                    while (!reservationOK) {
                        reservation = ValidationService.validateReservation(getInputReservation());
                        reservationOK = ValidationService.validateInProcessReservation(reservation);
                    }
                    String status = ValidationService.validateStatus(getInputStatus());
                    PrintService.printEditReservation(
                            ReservationService.editReservationWorkstage(reservationList, reservation, status));
                    backToSubMenu = false;
                    break;
                case -1:
                    System.out.println("input harus berupa angka antara 0 sampai 3\n");
                    break;
                case 0:
                    backToMainMenu = true;
                    break;
            }
        } while (!backToMainMenu);

    }

    public static String getInputStatus() {
        System.out.println("Selesaikan Reservasi");
        System.out.println("======Masukan (Finish/Canceled)======");
        String status = input.nextLine();
        return status;
    }

    public static Reservation getInputReservation() {
        PrintService.showRecentReservation(ReservationService.getRecentReservation(reservationList));
        System.out.println("Silahkan masukan Reservation Id");
        String reservationId = input.nextLine();
        return ReservationService.getReservationById(reservationList, reservationId);
    }

    private static List<Service> getListService(List<Service> serviceList2) {
        boolean finishServiceMenu = false;
        List<Service> newService = new ArrayList<>();
        while (!finishServiceMenu) {
            if (newService.size() == serviceList2.size()) { // cek apakah semua service telah terpenuhi
                System.out.println("======Semua service telah dipilih======");
                break;
            }
            Service service = ValidationService.validateService(getInputService()); // rekursif cek apakah service
                                                                                    // ditemukan
            boolean proceed = ValidationService.validateSameService(newService, service); // cek apakah service sudah
                                                                                          // dipilih
            if (!proceed) {
                System.out.println("======SERVICE sudah DIPILIH======");
                finishServiceMenu = false;
                continue;
            }
            newService.add(service);
            System.out.println("Ingin pilih service yang lain (Y/T) ?");
            String response = input.nextLine();
            finishServiceMenu = !response.equalsIgnoreCase("Y");
        }
        return newService;
    }

    public static Service getInputService() {
        PrintService.showAllService(ServiceService.getAllService(serviceList));
        System.out.println("Silahkan masukan Service Id");
        String serviceId = input.nextLine();
        return ServiceService.getServiceByServiceId(serviceList, serviceId);
    }

    public static Employee getInputEmployee() {
        PrintService.showAllEmployee(EmployeeService.getAllEmployee(personList));
        System.out.println("Silahkan masukan Employee Id");
        String employeeId = input.nextLine();
        return EmployeeService.getEmployeeByEmployeeId(personList, employeeId);
    }

    public static Customer getInputCustomer() {
        PrintService.showAllCustomer(CustomerService.getAllCustomer(personList));
        System.out.println("Silahkan masukan Customer Id");
        String customerId = input.nextLine();
        return CustomerService.getCustomerByCustomerId(personList, customerId);
    }
}
