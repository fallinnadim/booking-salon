package com.booking.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class PrintService {
        public static void printMenu(String title, String[] menuArr) {
                int num = 1;
                System.out.println(title);
                for (int i = 0; i < menuArr.length; i++) {
                        if (i == (menuArr.length - 1)) {
                                num = 0;
                        }
                        System.out.println(num + ". " + menuArr[i]);
                        num++;
                }
        }

        private static String printServices(List<Service> serviceList) {
                return serviceList
                                .stream()
                                .map(Service::getServiceName)
                                .collect(Collectors.joining(", "));
        }

        // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
        public static void showRecentReservation(List<Reservation> reservationList) {
                if (reservationList.size() == 0) {
                        System.out.println("+========================DATA TIDAK DITEMUKAN=========================+");
                        return;
                }
                System.out.printf("| %-4s | %-9s | %-15s | %-87s | %-15s | %-10s |\n",
                                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
                System.out
                                .println("+========================================================================================+");
                AtomicInteger number = new AtomicInteger();
                reservationList
                                .stream()
                                .forEach(element -> {
                                        number.incrementAndGet();
                                        System.out.printf("| %-4s | %-9s | %-15s | %-87s | %-15s | %-10s |\n",
                                                        number, element.getReservationId(),
                                                        element.getCustomer().getName(),
                                                        printServices(element.getServices()),
                                                        element.getReservationPrice(),
                                                        element.getWorkstage());
                                });

        }

        public static void showAllCustomer(List<Customer> customers) {
                System.out.printf("| %-4s | %-9s | %-11s | %-15s | %-15s | %-15s |\n",
                                "No.", "ID", "Nama", "Alamat", "Membership", "Uang");
                System.out
                                .println("+========================================================================================+");
                AtomicInteger number = new AtomicInteger();
                customers
                                .stream()
                                .forEach(element -> {
                                        number.incrementAndGet();
                                        System.out.printf("| %-4s | %-9s | %-11s | %-15s | %-15s | %-15s |\n",
                                                        number, element.getId(), element.getName(),
                                                        element.getAddress(),
                                                        element.getMember().getMembershipName(),
                                                        element.getWallet());
                                });
        }

        public static void showAllEmployee(List<Employee> employees) {
                System.out.printf("| %-4s | %-9s | %-11s | %-11s | %-15s |\n",
                                "No.", "Id", "Nama", "Alamat", "Pengalaman");
                System.out
                                .println("+========================================================================================+");
                AtomicInteger number = new AtomicInteger();
                employees
                                .stream()
                                .forEach(element -> {
                                        number.incrementAndGet();
                                        System.out.printf("| %-4s | %-9s | %-11s | %-11s | %-15s |\n",
                                                        number, element.getId(), element.getName(),
                                                        element.getAddress(),
                                                        element.getExperience());
                                });
        }

        public static void showHistoryReservation(List<Reservation> reservationList) {
                if (reservationList.size() == 0) {
                        System.out.println("+========================DATA TIDAK DITEMUKAN=========================+");
                        return;
                }
                System.out.printf("| %-4s | %-9s | %-15s | %-87s | %-15s | %-10s |\n",
                                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
                System.out
                                .println("+========================================================================================+");
                AtomicInteger number = new AtomicInteger();
                AtomicInteger totalKeuntungan = new AtomicInteger();
                reservationList
                                .stream()
                                .forEach(element -> {
                                        if (element.getWorkstage().equalsIgnoreCase("Finish")) {
                                                totalKeuntungan.addAndGet((int) element.getReservationPrice());
                                        }
                                        number.incrementAndGet();
                                        System.out.printf("| %-4s | %-9s | %-15s | %-87s | %-15s | %-10s |\n",
                                                        number, element.getReservationId(),
                                                        element.getCustomer().getName(),
                                                        printServices(element.getServices()),
                                                        element.getReservationPrice(),
                                                        element.getWorkstage());
                                });

                System.out.printf("\n %-15s | %-15s \n",
                                "Total Keuntungan", totalKeuntungan.doubleValue());
        }

        public static void showAllService(List<Service> allService) {
                System.out.printf("| %-4s | %-9s | %-21s | %-11s |\n",
                                "No.", "Id", "Nama", "Harga");
                System.out
                                .println("+========================================================================================+");
                AtomicInteger number = new AtomicInteger();
                allService
                                .stream()
                                .forEach(element -> {
                                        number.incrementAndGet();
                                        System.out.printf("| %-4s | %-9s | %-21s | %-11s |\n",
                                                        number, element.getServiceId(), element.getServiceName(),
                                                        element.getPrice());
                                });
        }

        public static void printEditReservation(String editReservationWorkstage) {
                System.out.println(editReservationWorkstage);
        }
}
