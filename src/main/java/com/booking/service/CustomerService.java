package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Person;

public class CustomerService {
    public static List<Customer> getAllCustomer(List<Person> personList) {
        return personList
                .stream()
                .filter(Customer.class::isInstance)
                .map(Customer.class::cast)
                .toList();
    }

    public static Customer getCustomerByCustomerId(List<Person> personList, String customerId) {
        return personList
                .stream()
                .filter(Customer.class::isInstance)
                .map(Customer.class::cast)
                .filter(element -> element.getId().equalsIgnoreCase(customerId))
                .findFirst()
                .orElse(null);
    }
}
