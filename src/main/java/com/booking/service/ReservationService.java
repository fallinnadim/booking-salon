package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {
    public static void createReservation(List<Reservation> reservationList, Customer customer, Employee employee,
            List<Service> newService) {
        Reservation reservation = Reservation.builder()
                .customer(customer)
                .employee(employee)
                .services(newService)
                .build();
        reservation.generateId();
        reservationList.add(reservation);
    }

    public static String editReservationWorkstage(List<Reservation> reservationList, Reservation reservation2,
            String status) {
        reservation2.setWorkstage(status);
        return "Reservasi dengan id " + reservation2.getReservationId() + " sudah " + status;
    }

    public static List<Reservation> getRecentReservation(List<Reservation> reservationList) {
        return reservationList
                .stream()
                .filter(element -> element.getWorkstage().equalsIgnoreCase("in process"))
                .toList();
    }

    public static List<Reservation> getHistoryReservation(List<Reservation> reservationList) {
        return reservationList;
    }

    public static Reservation getReservationById(List<Reservation> reservationList, String reservationId) {
        return reservationList
                .stream()
                .filter(element -> element.getReservationId().equalsIgnoreCase(reservationId))
                .findFirst()
                .orElse(null);
    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
