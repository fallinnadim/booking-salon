package com.booking.service;

import java.util.List;
import com.booking.models.Employee;
import com.booking.models.Person;

public class EmployeeService {
    public static List<Employee> getAllEmployee(List<Person> personList) {
        return personList
                .stream()
                .filter(Employee.class::isInstance)
                .map(Employee.class::cast)
                .toList();
    }

    public static Employee getEmployeeByEmployeeId(List<Person> personList, String employeeId) {
        return personList
                .stream()
                .filter(Employee.class::isInstance)
                .map(Employee.class::cast)
                .filter(element -> element.getId().equalsIgnoreCase(employeeId))
                .findFirst()
                .orElse(null);
    }
}
