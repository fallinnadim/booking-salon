package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
    public static void validateInput() {

    }

    public static Customer validateCustomer(Customer inputCustomer) {
        if (inputCustomer == null) {
            System.out.println("======CUSTOMER yang dicari TIDAK TERSEDIA======");
            inputCustomer = validateCustomer(MenuService.getInputCustomer());
        }
        return inputCustomer;
    }

    public static Employee validateEmployee(Employee inputEmployee) {
        if (inputEmployee == null) {
            System.out.println("======EMPLOYEE yang dicari TIDAK TERSEDIA======");
            inputEmployee = validateEmployee(MenuService.getInputEmployee());
        }
        return inputEmployee;
    }

    public static Service validateService(Service inputService) {
        if (inputService == null) {
            System.out.println("======SERVICE yang dicari TIDAK TERSEDIA======");
            inputService = validateService(MenuService.getInputService());
        }
        return inputService;
    }

    public static boolean validateSameService(List<Service> newService, Service service) {
        return newService.stream()
                .noneMatch(element -> element.getServiceId().equalsIgnoreCase(service.getServiceId()));
    }

    public static Reservation validateReservation(Reservation inputReservation) {
        if (inputReservation == null) {
            System.out.println("======Reservation yang dicari TIDAK TERSEDIA======");
            inputReservation = validateReservation(MenuService.getInputReservation());
        }
        return inputReservation;
    }

    public static String validateStatus(String inputStatus) {
        if (!(inputStatus.equalsIgnoreCase("Finish") || inputStatus.equalsIgnoreCase("Canceled"))) {
            inputStatus = validateStatus(MenuService.getInputStatus());
        }
        return inputStatus;
    }

    public static boolean validateInProcessReservation(Reservation reservation) {
        if (reservation.getWorkstage().equalsIgnoreCase("Finish")
                || reservation.getWorkstage().equalsIgnoreCase("Canceled")) {
            System.out.println("======Reservation yang dicari SUDAH SELESAI======");
            return false;
        }
        return true;
    }

    public static int validateInputMenu(String input, int maxNumberOfMenu) {
        int output = 0;
        try {
            output = Integer.parseInt(input);
        } catch (NumberFormatException e) {
            return -1;
        }
        if (0 <= output && output < maxNumberOfMenu + 1) return output;

        return -1;
    }
}
