package com.booking.models;

import java.util.List;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Reservation implements IDGenerator {
    private static int counter = 1;
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    @Builder.Default
    private String workstage = "In Process";
    // workStage (In Process, Finish, Canceled)

    public Reservation(Customer customer, Employee employee, List<Service> services,
            String workstage) {
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.reservationPrice = calculateReservationPrice();
        this.workstage = workstage;
    };

    private double calculateReservationPrice() {
        int percentage = 0;
        double reservationPrice = services.stream().collect(Collectors.summingDouble(Service::getPrice));
        if (this.getCustomer().getMember().getMembershipName().equalsIgnoreCase("Gold")) {
            percentage = 10;
        }
        if (this.getCustomer().getMember().getMembershipName().equalsIgnoreCase("Silver")) {
            percentage = 5;
        }
        return reservationPrice - (reservationPrice * percentage / 100);
    }

    @Override
    public void generateId() {
        int i = Reservation.counter++;
        String prefix = "Rsv-";
        String id = String.valueOf(i);
        for (int j = 0; j < 2 - id.length(); j++) { // 2 digit
            prefix += "0";
        }
        prefix += id;
        setReservationId(prefix);
    }
}
