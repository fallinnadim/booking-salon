package com.booking.models;

public interface IDGenerator {
    void generateId();
}
